# Understanding Channels

## Tips 
goroutines
: to execute tasks independently, potentially in parallel.

channels
: for communication, synchronization between goroutines.


channels are inherently interesting:
- goroutine-safe
- store and pass value between goroutines
- provide FIFO semantics
- can cause goroutines to block and unblock

##### Make a channel
buffered channel
: `ch := make(chan int, 3)`

### hchan struct
![hchan-struct](./assets/hchan-struct.png)

#### hchan after first send
![hchan-after-first-send](./assets/hchan-after-first-send.png)

#### hchan after three sends
![hchan-after-three-sends](./assets/hchan-after-three-sends.png)

#### hchan after first receive
![hchan-after-first-receive](./assets/hchan-after-first-receive.png)

The `ch := make(chan int, 3)` allocates an hchan struct on the heap, initializes it and returns a pointer to it.
![hchan-on-heap](./assets/hchan-on-heap.png)

This is why we can pass channels between functions, don't need to pass ``pointers to channels``.
![passing-channels-to-functions](./assets/passing-channels-to-functions.png)

#### Sends and Receives
<br>

![channel-sends-and-receives-1](./assets/channel-sends-and-receives-1.png)
![channel-sends-and-receives-2](./assets/channel-sends-and-receives-2.png)
![channel-sends-and-receives-3](./assets/channel-sends-and-receives-3.png)
![channel-sends-and-receives-4](./assets/channel-sends-and-receives-4.png)
![channel-sends-and-receives-5](./assets/channel-sends-and-receives-5.png)
![channel-sends-and-receives-6](./assets/channel-sends-and-receives-6.png)
![channel-sends-and-receives-7](./assets/channel-sends-and-receives-7.png)
![channel-sends-and-receives-8](./assets/channel-sends-and-receives-8.png)
![channel-sends-and-receives-9](./assets/channel-sends-and-receives-9.png)
![channel-sends-and-receives-10](./assets/channel-sends-and-receives-10.png)
![channel-sends-and-receives-11](./assets/channel-sends-and-receives-11.png)
![channel-sends-and-receives-12](./assets/channel-sends-and-receives-12.png)
![channel-sends-and-receives-13](./assets/channel-sends-and-receives-13.png)
![channel-sends-and-receives-14](./assets/channel-sends-and-receives-14.png)
![channel-sends-and-receives-15](./assets/channel-sends-and-receives-15.png)
![channel-sends-and-receives-16](./assets/channel-sends-and-receives-16.png)
![channel-sends-and-receives-17](./assets/channel-sends-and-receives-17.png)
![channel-sends-and-receives-18](./assets/channel-sends-and-receives-18.png)
![channel-sends-and-receives-19](./assets/channel-sends-and-receives-19.png)
![channel-sends-and-receives-20](./assets/channel-sends-and-receives-20.png)
![channel-sends-and-receives-21](./assets/channel-sends-and-receives-21.png)
![channel-sends-and-receives-22](./assets/channel-sends-and-receives-22.png)
![channel-sends-and-receives-23](./assets/channel-sends-and-receives-23.png)
![channel-sends-and-receives-24](./assets/channel-sends-and-receives-24.png)
![channel-sends-and-receives-25](./assets/channel-sends-and-receives-25.png)
![channel-sends-and-receives-26](./assets/channel-sends-and-receives-26.png)
![channel-sends-and-receives-27](./assets/channel-sends-and-receives-27.png)
![channel-sends-and-receives-28](./assets/channel-sends-and-receives-28.png)
![channel-sends-and-receives-29](./assets/channel-sends-and-receives-29.png)
![channel-sends-and-receives-30](./assets/channel-sends-and-receives-30.png)
![channel-sends-and-receives-31](./assets/channel-sends-and-receives-31.png)
![channel-sends-and-receives-32](./assets/channel-sends-and-receives-32.png)
![channel-sends-and-receives-33](./assets/channel-sends-and-receives-33.png)
![channel-sends-and-receives-34](./assets/channel-sends-and-receives-34.png)
![channel-sends-and-receives-35](./assets/channel-sends-and-receives-35.png)
![channel-sends-and-receives-36](./assets/channel-sends-and-receives-36.png)
![channel-sends-and-receives-37](./assets/channel-sends-and-receives-37.png)
![channel-sends-and-receives-38](./assets/channel-sends-and-receives-38.png)
![channel-sends-and-receives-39](./assets/channel-sends-and-receives-39.png)
![channel-sends-and-receives-40](./assets/channel-sends-and-receives-40.png)
![channel-sends-and-receives-41](./assets/channel-sends-and-receives-41.png)
![channel-sends-and-receives-42](./assets/channel-sends-and-receives-42.png)
![channel-sends-and-receives-43](./assets/channel-sends-and-receives-43.png)
![channel-sends-and-receives-44](./assets/channel-sends-and-receives-44.png)
![channel-sends-and-receives-45](./assets/channel-sends-and-receives-45.png)
![channel-sends-and-receives-46](./assets/channel-sends-and-receives-46.png)
![channel-sends-and-receives-47](./assets/channel-sends-and-receives-47.png)
![channel-sends-and-receives-48](./assets/channel-sends-and-receives-48.png)

<br>
unbuffered channel
: `ch := make(chan int)`

### Simplicity and Performance

![simplicity-and-performance-1](./assets/simplicity-and-performance-1.png)
![simplicity-and-performance-2](./assets/simplicity-and-performance-2.png)
![simplicity-and-performance-3](./assets/simplicity-and-performance-3.png)
![simplicity-and-performance-4](./assets/simplicity-and-performance-4.png)
![simplicity-and-performance-5](./assets/simplicity-and-performance-5.png)

## References
- [Youtube - GopherCon 2017: Kavya Joshi](https://www.youtube.com/watch?v=KBZlN0izeiY)